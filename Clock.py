import pygame
import time
import math

#Khoi tao pygame
pygame.init()

# tao khung hien thi ung dung
screen= pygame.display.set_mode((400,500))

# bell 
sound =  pygame.mixer.Sound('bell.mp3')

# color
BLUE = (8,69,166)
WHITE= (255,255,255)
BORDER_TIMELINE= (0,50,100)
BLACK=(0,0,0)
RED = (255,0,0)

# creat font to icon, text ....
font =pygame.font.SysFont('sans',25)
fontSymBol=pygame.font.SysFont('sans',40)
font_point_0 =pygame.font.SysFont('sans',12)

# text 
tex_point_0=font_point_0.render('0',True,BLACK)
icon_plus_minute= fontSymBol.render('+', True, BLACK)
icon_plus_second= fontSymBol.render('+', True, BLACK)
icon_minus_minute= fontSymBol.render('-', True, BLACK)
icon_minus_second= fontSymBol.render('-', True, BLACK)
Start_button= font.render('START', True, BLACK)
End_button= font.render('END', True, BLACK)

# frame
running=True

# const
total_seconds = 0 
start = False
total = 0


clock = pygame.time.Clock()
# Vong lap cho man hinh ung dung hien thi lien tung
while running:
	clock.tick(60)
	#  vẽ gì trước thì sẽ hiển thị trước

	screen.fill(BLUE) 

# 	get position to click on button or textbox
	mouse_x, mouse_y = pygame.mouse.get_pos()
	
	
# draw tọa độ x,y kích thước xy
	# textbox time 
		# icon_plus_minute
	pygame.draw.rect(screen,WHITE,(75,50,50,50))
		# icon_plus_second
	pygame.draw.rect(screen,WHITE,(75, 150 , 50, 50))
		# icon_minus_minute
	pygame.draw.rect(screen,WHITE,(150, 50, 50, 50))
		# icon_minus_second
	pygame.draw.rect(screen,WHITE,(150, 150, 50, 50))



# /////////////////////////////////////
	# icon + -
	screen.blit(icon_plus_minute,(90,50))
	screen.blit(icon_plus_second,(165,50))
	screen.blit(icon_minus_minute,(90,150))
	screen.blit(icon_minus_second,(165,150))
	
# /////////////////////////////
	# start button
	pygame.draw.rect(screen,WHITE,(225, 60, 100, 30))
	# end button 
	pygame.draw.rect(screen,WHITE,(225, 160, 100, 30))
	#text startbutton
	screen.blit(Start_button,(235,60))
	# text endbutton
	screen.blit(End_button,(235,160))


# //////////////////////////////
	#border timeline
	pygame.draw.rect(screen,BORDER_TIMELINE,(65, 390, 270, 50))
	# timeline
	pygame.draw.rect(screen,WHITE,(75, 400, 250, 30))

	# border circle
	pygame.draw.circle(screen,BLACK,(200,295),75)
	# circle
	pygame.draw.circle(screen,WHITE,(200,295),70)

	# center point
	pygame.draw.circle(screen,BLACK,(200,295),3)

	# point 0
	# pygame.draw.line(screen,BLACK,(200,295),(200,230))
	screen.blit(tex_point_0,(197.9,230))

	

	

	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			running = False

		if event.type == pygame.MOUSEBUTTONDOWN:
			if event.button == 1:
				pygame.mixer.pause()

				# click on icon + "one minute"
				if(75< mouse_x<125 and 50<mouse_y <100):
					total_seconds += 60
					total =total_seconds
					

				# click on icon 2
				if(150< mouse_x<200 and 50<mouse_y <100):
					total_seconds += 1
					total =total_seconds
					

				# click on icon - 'one minute'
				if(75< mouse_x<125 and 150<mouse_y <200):
					total_seconds -= 60
					if(total_seconds<0):
						total_seconds =0
						total =total_seconds
					
				
				# click on icon 4	
				if(150< mouse_x<200 and 150<mouse_y <200):
					total_seconds -= 1
					if(total_seconds<0):
						total_seconds =0
					
				# click on starbutton 	
				if(225< mouse_x<325 and 60<mouse_y <90):
					total = total_seconds
					start = True



					
				# click on end button	
				if(225< mouse_x<325 and 160<mouse_y <190):
					start = False
					total_seconds = 0
				


	# if click on start button, total_seconds redue time to 0
	if start:
		total_seconds -= 1
		if total_seconds <= 0:
			start = False
			total_seconds = 0
			pygame.mixer.Sound.play(sound)
		time.sleep(0.01)	
				
	# logic timedisplay.
	minute = int(total_seconds/60)
	seconds = total_seconds - minute*60
	

	# text time display
	text_minutes=str(minute)
	text_seconds=str(seconds)


	text_time=font.render(text_minutes,True,BLACK)
	screen.blit(text_time,(95,112.5))

	text_time2=font.render(text_seconds,True,BLACK)
	screen.blit(text_time2,(170,112.5))

	text_icon=font.render(':',True,BLACK)
	screen.blit(text_icon,(137.5,112.5))
	# recipe to get Xc Yc of second clockwise and minute clockwise:
		# xc = 200 + r*sina
		# yc = 295 - r*cosa
		# r= 65, a = 6*seconds
	# second clockwise
	Xsecond = 200 + 50*math.sin(6*seconds*math.pi/180)
	Ysecond = 295 - 50*math.cos(6*seconds*math.pi/180)
	pygame.draw.line(screen,RED,(200,295),(int(Xsecond),int(Ysecond)))

	# minute clockwise
	Xminute = 200 + 30*math.sin(6*minute*math.pi/180)
	Yminute = 295 - 30*math.cos(6*minute*math.pi/180)
	pygame.draw.line(screen,BLACK,(200,295),(Xminute,int(Yminute)))

	if total!=0:
		pygame.draw.rect(screen,RED,(75, 400, int(250*(total_seconds/total)), 30))

	pygame.display.flip()

pygame.quit()